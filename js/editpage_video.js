import { clickSideMenu, createSideMenuBar } from './api/sidemenu.js';
import download from './api/download.js';
import createTimeline from './api/timeline.js';

clickSideMenu(); // 사이드 메뉴 클릭시 동작
createSideMenuBar(); // 해당 메뉴바 생성
download(); // 다운로드 기능
createTimeline(); // 타임라인 유닛 생성

