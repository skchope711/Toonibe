import download from './api/download.js';
import { clickSideMenu, createSideMenuBar } from './api/sidemenu.js';
import clickUploadBtn from './api/imageupload.js'
import { copyBasicCampus } from './api/navigation.js'

download(); // 다운로드 기능
clickSideMenu(); // 사이드 메뉴 클릭 이벤트
createSideMenuBar(); // 사이드 메뉴바 생성 함수(active 클래스 포함된 메뉴의 메뉴바 생성)
clickUploadBtn(); // 이미지 업로드 기능
copyBasicCampus(); // 처음 화면 캔버스 네비게이션 복사







