export default function downloadImage() {

    const modalWrapper = document.querySelector(".modal_wrapper"); // 모달창 래퍼
    const modal = document.querySelector(".modal") // 모달창
    const downloadBtn = document.getElementById('download') // 다운로드 버튼
    const closeBtn = document.querySelector('.close_btn'); // 닫기 버튼
    const cancelBtn = document.querySelector('.btn.cancel'); // 취소 버튼
    const completeBtn = document.querySelector('.btn.complete'); // 다운로드 완료 버튼
    const canvas = document.getElementById('canvas'); // 캔버스

    // 다운로드 버튼 클릭시 모달창 열기
    downloadBtn.addEventListener("click", function() { 
        modalWrapper.classList.remove("off"); 
        completeBtn.addEventListener("click", downloadMultiImage)
    })

    // 모달창 닫기
    // 모달 래퍼 클릭시 모달창 닫기
    modalWrapper.addEventListener("click", function() {
        modalWrapper.classList.add("off"); 
    })

    // 모달창 클릭시 모달창 켠상태 유지
    modal.addEventListener('click', function(e) {
        e.stopPropagation();
    })

    // 닫기 버튼 클릭시 모달창 닫기
    closeBtn.addEventListener("click", function() { 
        modalWrapper.classList.add("off"); 
    })

    // 취소 버튼 클릭시 모달창 닫기
    cancelBtn.addEventListener("click", function() {
        modalWrapper.classList.add("off"); 
    })
    
}

// 다중 이미지 한개의 파일로 압축해서 다운로드 함수
function downloadMultiImage() {

    let totalCanvas = document.getElementById('canvas_wrapper').children; // 캔버스 전체
    let imgGroup = []; // 다운받을 이미지 전체

    const fileType = file_type.value; // 이미지 확장자
    const fileName = file_name.value.trim(); // 이미지 저장명
    const fileResolution = file_resolution.value; // 이미지 해상도
    imgGroup = []; // imgGroup 배열 정보 비우기
    for(let i = 0; i < totalCanvas.length; i++) {
        imgGroup.push(totalCanvas[i].toDataURL(`image/${fileType}`, [0.0, 1.0]).split(',')[1]);
    }

    let zip = new JSZip();
    let img = zip.folder(`Webtoons`);
    for(let i = 0; i < imgGroup.length; i++) {
        let file = "img" + (i + 1) + "." + fileType;
        img.file(file, imgGroup[i], {base64: true});
    }
    
    zip.generateAsync({type:"blob"}).then(function(content) {
        saveAs(content, `${fileName}.zip`);
    });  
    
}