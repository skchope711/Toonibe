import controlCanvasOpacity from './opacity.js'
import { synchronizeNavigation } from './navigation.js'

// 이미지 버튼 클릭시 동작 함수
export default function clickUploadBtn() { 
    let upload = document.getElementById('upload');
    upload.addEventListener("change", uploadImage);
}

// 이미지 업로드 함수
function uploadImage(event) {

    let implementbar = document.getElementById('image_list'); // 이미지 리스트
    implementbar.innerHTML = ``; // 업로드 전 이미지 리스트 비우기
    // 업로드 전 캔버스 기본상태로 세팅
    document.querySelector('#canvas_wrapper').innerHTML = ` 
        <canvas id="canvas" width="690px" height="9660px"></canvas>
    `;

    const canvas = document.getElementById("canvas");
    const context = canvas.getContext("2d");

    completeImageUpload(event).then(function(){
        synchronizeNavigation(); // 네비게이션 복사 및 연동
    });
    
}

// 페이지 추가 함수
function addCanvas(pageNum) {
    let canvasWrapper = document.getElementById('canvas_wrapper');
    let newCanvas = `<canvas width="690px" height="9660px"></canvas>`;
    for(let i = 1; i < pageNum; i++) {
        canvasWrapper.innerHTML += newCanvas;
    }
} 

// 이미지 업로드 완료 함수
function completeImageUpload(event) {
    return new Promise(function(resolve){
        let imageNum = 0; // 이미지 번호
        let basicMargin = 800; // 이미지간 간격
        let totalImageNumber = event.target.files.length; // 업로드 이미지 개수
        for (var image of event.target.files) { 
            var reader = new FileReader(); 
            reader.onload = function(event) {
                // 리스트에 이미지 요소 추가
                let uploadImage = `
                    <div class="cartoon">
                        <div class="number">Layer-${imageNum += 1}</div>
                        <div class="image">
                            <img src="${event.target.result}" alt="만화 이미지">
                        </div>
                    </div>
                `;
                document.querySelector('#image_list').innerHTML += uploadImage;
    
                // 캔버스에 이미지 요소 추가
                let imageElem = new Image();
                imageElem.src = event.target.result;       
                imageElem.number = imageNum; // 이미지 번호 넘기기
                imageElem.onload = function() { // 이미지 다운로드 완료 이후
                    let totalPage; // 전체 캔버스 수
                    // 전체 페이지 수 계산
                    if(imageElem.number == totalImageNumber) {
                        let totalHeight = 100 + basicMargin * (totalImageNumber - 1) + imageElem.height; // 이미지 전체 길이
                        totalPage = Math.ceil(totalHeight / canvas.height); // 전체 페이지 수
                    }
                    // 전체 페이지 생성
                    if(totalPage > 1) {
                        addCanvas(totalPage);
                    } 
                    // 캔버스 전체 흰색으로 칠하기
                    let totalCanvas = document.getElementById('canvas_wrapper').children; 
                    for(let i = 0; i < totalCanvas.length; i++) {
                        let context = totalCanvas[i].getContext('2d');
                        context.fillStyle = '#fff'; 
                        context.fillRect(0, 0, totalCanvas[i].width, totalCanvas[i].height);
                    }
                    // 업로드 이미지 캔버스에 그리기
                    setTimeout(function() {
                       let imageHeight = 100 + basicMargin * (imageElem.number - 1) + imageElem.height;
                       let selectCanvasNum = Math.ceil(Number(imageHeight) / 9660) - 1; 
                       let totalCanvas = document.getElementById('canvas_wrapper').children; 
                       let selectedCanvas = totalCanvas[selectCanvasNum]; 
                       let ctx = selectedCanvas.getContext('2d');
                       let positionX = (selectedCanvas.width - imageElem.width) / 2; // 이미지 왼쪽상단 x좌표
                       let positionY = basicMargin * (imageElem.number - ( 12 * selectCanvasNum ) - 1) + 100; // 이미지 왼쪽상단 y좌표
                       ctx.drawImage(imageElem, positionX, positionY, imageElem.width, imageElem.height);
                       controlCanvasOpacity(); // 캔버스 투명도 조절
                    }, 500)
                }
            };  
            reader.readAsDataURL(image); 
        } 
        resolve();
    });
}

