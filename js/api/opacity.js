// 캔버스 투명도 조절 함수(캔버스, 캔버스 API 초기화, 이미지)
export default function controlCanvasOpacity() {

    let opacity = document.getElementById('opacity'); 
    let opacityBar = opacity.querySelector('input'); // 투명도 조절바

    let canvasGroup = document.getElementById('canvas_wrapper').children;
    let imageElems = [] // 캔버스 이미지 그룹
    for(let i = 0; i < canvasGroup.length; i++) {
        let imageUrl = canvasGroup[i].toDataURL();
        let imageElem = new Image();
        imageElem.src = imageUrl;
        imageElem.onload = function() {
            imageElems.push(imageElem);
        }
    }

    // 캔버스 이미지 업로드 완료되고 나서, 투명도 효과 적용해야 하므로 비동기로 처리한다.
    setTimeout(function() {
        opacityBar.addEventListener("input", function(e) {
            let totalCanvas = document.getElementById('canvas_wrapper').children;
            let opacityValue = e.target.value / 100; // 투명도 효과 0 ~ 100을 0 ~ 1의 범위로 변환
            for(let i = 0; i < totalCanvas.length; i++) {
                let myContext = totalCanvas[i].getContext('2d');
                myContext.clearRect(0, 0, totalCanvas[i].width, totalCanvas[i].height); // 캔버스 지우기
                myContext.globalAlpha = opacityValue; // 캔버스 투명효과 적용
                myContext.drawImage(imageElems[i], 0, 0, totalCanvas[i].width, totalCanvas[i].height); // 투명효과 적용된 캔버스 이미지 그리기
            }   
        })
    }, 100)
}



