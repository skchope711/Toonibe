// 타임라인 유닛 생성
export default function createTimelineUnit() {

    let timelineUnit = document.getElementById('timeline_unit');
    let effectTime = 100; // 효과 지속 시간
    for(let i = 0; i < effectTime; i += 2) {
        let frame = `
            <div class="frame">
                <div class="number">00:${i < 10 ? '0' + i : i}</div>
                <div class="bar">
                    <div class="long"></div>
                    <div class="short"></div>
                    <div class="short"></div>
                    <div class="short"></div>
                    <div class="short"></div>
                    <div class="short"></div>
                    <div class="short"></div>
                    <div class="short"></div>
                    <div class="short"></div>
                    <div class="short"></div>
                </div>
            </div>
        `; // 프레임 단위
        timelineUnit.innerHTML += frame;
    }

}
