// 네비게이션과 캔버스 스크롤 동작 연동 함수
export function synchronizeNavigation() {
    let canvasWrapper = document.getElementById("canvas_wrapper"); // 캔버스 래퍼
    let currentPos = document.querySelector(".current_pos"); // 네비게이션 내 현재 위치
    let currentDisplayTop; // 현재 보고 있는 화면 위쪽 위치
    let currentDisplayBottom; // 현재 보고 있는 화면 아래쪽 위치
    let tempPage = 0; // 현재 페이지 기억
    let status = false; // 캔버스 복사 한번만 시행

    canvasWrapper && canvasWrapper.addEventListener("scroll", function() {
        if(!status) {
            copyCanvasImage(0);
            status = true;
        }
        let scrollValue = canvasWrapper.scrollTop; // 스크롤된 문서 길이
        let currentPage = Math.floor(scrollValue / 9680);
        if(currentPage !== tempPage) copyCanvasImage(currentPage);
        let proportionValue = (scrollValue - (9680 * currentPage)) / 8660 * 100;
        if(proportionValue > 100) proportionValue = 100; 
        let newProportionValue = proportionValue * 89.6 / 100; // 0 ~ 100의 범위를 0 ~ 89.6의 범위로 좁힌것 
        currentDisplayTop = currentPos.scrollHeight * newProportionValue / 100;
        currentDisplayBottom = 116 + currentPos.scrollHeight * newProportionValue / 100;
        currentPos.style.clip = "rect(" + currentDisplayTop + "px, 80px, " + currentDisplayBottom + "px, 0)";
        tempPage = currentPage;
    })
}

// 캔버스 이미지를 네비게이션으로 복사 함수
export function copyCanvasImage(currentPage) {
    let canvasWrapper = document.getElementById("canvas_wrapper"); // 캔버스 래퍼
    let totalCanvas = canvasWrapper.children; // 래퍼내 전체 캔버스
    let selectedCanvas = totalCanvas[currentPage];
    let navigationBackImg = document.getElementById('back_img'); // 네비게이션 배경 이미지
    let navigationFrontImg = document.getElementById('front_img'); // 네비게이션 현재 보이는 화면 이미지
    navigationBackImg.src = selectedCanvas.toDataURL(); // 캔버스 배경 이미지 할당
    navigationFrontImg.src = selectedCanvas.toDataURL(); // 캔버스 현재 보이는 화면 이미지 할당
}

// 처음 화면 캔버스 네비게이션 복사
export function copyBasicCampus() {
    let canvas = document.getElementById('canvas'); 
    let ctx = canvas.getContext('2d');
    ctx.fillStyle = '#fff'; 
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    copyCanvasImage(0); // 기본 캔버스 복사
    synchronizeNavigation(); // 네비게이션 연동 기능
}
