// 캔버스 이미지 자르기
export function cropCanvasImage(canvas, ctx, img) {
    
}

// 캔버스 이미지 확대/축소 
export function zoomCanvasImage(canvas, ctx, img) {
    let zoomIn = document.getElementById('zoom_in');
    let zoomOut = document.getElementById('zoom_out');
    let canvasWrapper = document.getElementById('canvas_wrapper');
    let statusNum = 0; // 확대상태면 +, 축소상태면 -
    let canvasHalfWidth = canvasWrapper.offsetWidth / 2;
    let canvasHalfHeight = canvasWrapper.offsetHeight / 2;
    zoomIn.addEventListener("click", function() { // 확대 클릭시 캔버스 2배 확대
        if (statusNum < 0) { // 화면 축소가 1회 이상 진행된 경우
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            ctx.scale(1.25, 1.25);
            ctx.drawImage(img, 0, 0, canvas.width * 1.25, canvas.height * 1.25); // 캔버스에 이미지 그리기
        } else if (statusNum >= 0) { // 화면 기본상태거나 확대가 1회이상 진행된 경우
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            canvas.width = 1.25 * canvas.width;
            canvas.height = 1.25 * canvas.height;
            ctx.scale(1.25, 1.25);
            ctx.drawImage(img, 0, 0, canvas.width * 0.8, canvas.height * 0.8); // 캔버스에 이미지 그리기
        }
        statusNum++;
        console.log(statusNum);
    })
    zoomOut.addEventListener("click", function() { // 축소 클릭시 캔버스 2배 축소
        if(statusNum >= 1) { // 화면 확대가 1회이상 진행된 경우
            ctx.clearRect(0, 0, canvas.width, canvas.height); // 캔버스 지우기
            canvas.width = 0.8 * canvas.width;
            canvas.height = 0.8 * canvas.height;
            ctx.scale(0.8, 0.8);
            ctx.drawImage(img, 0, 0, 1.25 * canvas.width, 1.25 * canvas.height); // 캔버스에 이미지 그리기
        } else if (statusNum <= 0) { // 화면 기본상태거나 화면 축소가 1회이상 진행된 경우
            ctx.clearRect(0, 0, canvas.width * 1.25, canvas.height * 1.25);
            ctx.scale(0.8, 0.8);
            ctx.drawImage(img, 0, 0, canvas.width * 1.25, canvas.height * 1.25); // 캔버스에 이미지 그리기
        }
        statusNum--;
        console.log(statusNum);
    })
}

// 캔버스 이미지 삭제
export function deleteCanvasImage(canvas, ctx) {
    let deleteBtn = document.getElementById('delete');
    deleteBtn.addEventListener("click", function() {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        const dataUrl = canvas.toDataURL(); // 캔버스 이미지 base64 문자화
        let navigationBackImg = document.getElementById('back_img'); // 네비게이션 배경 이미지
        let navigationFrontImg = document.getElementById('front_img'); // 네비게이션 현재 보이는 화면 이미지
        navigationBackImg.src = dataUrl; // 캔버스 배경 이미지 할당
        navigationFrontImg.src = dataUrl; // 캔버스 현재 보이는 화면 이미지 할당
    })
}


